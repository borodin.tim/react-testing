import React, { useState, Fragment } from 'react';
import Output from './Output';

const Greeting = () => {
    const [changeText, setChangeText] = useState(false);

    const changeTextHandler = () => {
        setChangeText(true);
    };

    return (
        <Fragment>
            <div>Greetings!</div>
            {!changeText && <Output>It's great to see you</Output>}
            {changeText && <div>Changed!</div>}
            <button
                onClick={changeTextHandler}
            >Change text</button>
        </Fragment>
    );
};

export default Greeting;