import { render, screen } from '@testing-library/react';
import { fireEvent } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Greeting from './Greeting';

describe('Greeting component', () => {
    test('renders greetings as a text', () => {
        render(<Greeting />);
        const textElement = screen.getByText('greeting', { exact: false });
        expect(textElement).toBeInTheDocument();
    });
    test('renders "great to see you" text when button is not clicked', () => {
        render(<Greeting />);

        const textElement = screen.getByText('great to see you', { exact: false });
        expect(textElement).toBeInTheDocument();
    });
    test('renders a "Changed!" when button was clicked', () => {
        render(<Greeting />);

        const textElement = screen.getByText('great to see you', { exact: false });
        expect(textElement).toBeInTheDocument();

        const btn = screen.getByText('Change text');
        // const btn = screen.getByRole('button');
        // fireEvent.click(btn);
        userEvent.click(btn);

        const changedTextElement = screen.getByText('Changed!');
        expect(changedTextElement).toBeInTheDocument();
    });
    test('dones not render the original text "great to see you" when button was clicked', () => {
        render(<Greeting />);

        const textElement = screen.getByText('great to see you', { exact: false });
        expect(textElement).toBeInTheDocument();

        const btn = screen.getByText('Change text');
        userEvent.click(btn);

        const changedTextElement = screen.queryByText('great to see you', { exact: false });
        expect(changedTextElement).toBeNull();
    });
});